# SUIF

A simple user interface for FPGA, used in XDU teaching, written in Rust and [Slint](https://slint.rs) for GUI.
